package com.example.dell.testsearchview;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imageView, imageView2;
    String path = " http://media1.tinngan.vn/archive/images/2015/10/15/191732_Ronaldo2.jpg";
    com.squareup.picasso.LruCache cache;
    Button btnSaveSQL, btnGetSQL;
    byte[] buffer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.imageView);
        imageView2 = (ImageView) findViewById(R.id.imageView2);

        btnSaveSQL = (Button) findViewById(R.id.buttonSaveSQL);
        btnSaveSQL.setOnClickListener(this);

        btnGetSQL = (Button) findViewById(R.id.buttonGetSQL);
        btnGetSQL.setOnClickListener(this);

        showImage(path);
       /* cache = new com.squareup.picasso.LruCache(10 * 1024 * 1024);
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.memoryCache(cache);
        builder.build().with(this).load(url).into(target);
        if (cache.get("bitmap1") != null) {
            System.out.println("co roi");
        } else {
            System.out.println("ko co");
        }*/
    }

    private void showImage(String s) {
        AsyncTask<String, Void, Bitmap> asyncTask = new AsyncTask<String, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(String... params) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);

                    connection.connect();
                    InputStream inputStream = connection.getInputStream();

                    buffer = new byte[connection.getContentLength()];
                    byte[] arr = new byte[100];
                    int index = 0;
                    int len = 0;

                    while ((len = inputStream.read(arr)) != -1) {
                        for (int i = 0; i < len; i++) {
                            buffer[index] = arr[i];
                            index++;
                        }
                    }

                    Bitmap bitmap = BitmapFactory.decodeByteArray(buffer, 0, connection.getContentLength());
                    inputStream.close();

                    return bitmap;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                imageView.setImageBitmap(bitmap);
            }
        };
        asyncTask.execute(new String[]{s});
    }

    Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            System.out.println(from.toString());
            cache.set("bitmap1", bitmap);
            System.out.println("chieu dai la " + cache.size());
            System.out.println("chieu dai la " + cache.maxSize());
            imageView.setImageBitmap(bitmap);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher));
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSaveSQL:
                saveSQL();
                break;
            case R.id.buttonGetSQL:
                getSQL();
                break;
            default:
                break;
        }
    }

    private void saveSQL() {
        Toast.makeText(MainActivity.this, "Da luu", Toast.LENGTH_SHORT).show();
        ImageDatabase database = new ImageDatabase(this);
        SQLiteDatabase db = database.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("BytesofImage", buffer);
        db.insert("Table1", null, values);
    }

    private void getSQL() {
        Toast.makeText(MainActivity.this, "Da lay", Toast.LENGTH_SHORT).show();
        ImageDatabase database = new ImageDatabase(this);
        SQLiteDatabase db = database.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from Table1", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            byte[] arr = cursor.getBlob(1);
            Bitmap bitmap = BitmapFactory.decodeByteArray(arr, 0, arr.length);
            imageView.setImageBitmap(bitmap);
            cursor.moveToNext();
        }
    }
}
