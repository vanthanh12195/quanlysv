package com.example.dell.testsearchview;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class ImageDatabase extends SQLiteOpenHelper {
    private static final String DATABASENAME = "sql12";
    private static final int DATABASEVERSION = 1;

    public ImageDatabase(Context context) {
        super(context, DATABASENAME, null, DATABASEVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        System.out.println("onCreate");
        String sql1 = "CREATE TABLE [Table1] (Id integer NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,BytesofImage blob NOT NULL)";
        db.execSQL(sql1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        System.out.println("onUpgrade");
    }
}
