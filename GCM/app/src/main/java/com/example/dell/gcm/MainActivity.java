package com.example.dell.gcm;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";
    Button btnDangKy, btnClearCache;
    TextView txtToken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnDangKy = (Button) findViewById(R.id.buttonDangky);
        btnDangKy.setOnClickListener(this);

        btnClearCache = (Button) findViewById(R.id.buttonClearCache);
        btnClearCache.setOnClickListener(this);

        txtToken = (TextView) findViewById(R.id.textViewToken);

        /*if (checkPlayServices()) {
            System.out.println("Trong máy có Google Plays Service");
            // Start IntentService to register this application with GCM.
            if (token != null) {
                System.out.println("da dang ky");
                System.out.println("token trong mainactivity la " + token);
                System.out.println(check);
                // da dang ky roi
                if (!check) {
                    System.out.println("chua gui dk");
                    // gui lai len server
                }
            } else {
                System.out.println("chua dang ky");
                // chua dang ky
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);
            }
        } else {
            System.out.println("Trong máy không có Google Plays Service");
        }*/
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonDangky:
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                String token = sharedPreferences.getString("thanh", "null");

                txtToken.setText(token);

                break;
            case R.id.buttonClearCache:

                SharedPreferences sharedPreferences1 = PreferenceManager.getDefaultSharedPreferences(this);
                sharedPreferences1.edit().remove("thanh").apply();


                String token1 = sharedPreferences1.getString("thanh", "null");

                txtToken.setText(token1);

                break;
            default:
                break;
        }
    }
}
