package com.example.dell.xephinh;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    ImageView imgPiece1, imgPiece2, imgPiece3, imgPiece4, imgPiece5, imgPiece6, imgPiece7, imgPiece8, imgPiece9;
    ImageView img1, img2, img3, img4, img5, img6, img7, img8, img9;
    int status = 0;
    ArrayList<ImageView> arrayList;
    ArrayList<ImageView> arrayListPiece;
    int i;
    ToneGenerator tg;
    ArrayList<ImageView> arrayListCheck;
    int count = 0;
    TextView txtCongratulation;
    Chronometer chronometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chronometer = (Chronometer) findViewById(R.id.chronometerTime);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();

        arrayListCheck = new ArrayList<>();
        tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

        txtCongratulation = (TextView) findViewById(R.id.textViewCongratulation);
        txtCongratulation.setVisibility(View.INVISIBLE);


        imgPiece1 = (ImageView) findViewById(R.id.imageViewPiece1);
        imgPiece2 = (ImageView) findViewById(R.id.imageViewPiece2);
        imgPiece3 = (ImageView) findViewById(R.id.imageViewPiece3);
        imgPiece4 = (ImageView) findViewById(R.id.imageViewPiece4);
        imgPiece5 = (ImageView) findViewById(R.id.imageViewPiece5);
        imgPiece6 = (ImageView) findViewById(R.id.imageViewPiece6);
        imgPiece7 = (ImageView) findViewById(R.id.imageViewPiece7);
        imgPiece8 = (ImageView) findViewById(R.id.imageViewPiece8);
        imgPiece9 = (ImageView) findViewById(R.id.imageViewPiece9);

        arrayListPiece = new ArrayList<>();
        arrayListPiece.add(imgPiece1);
        arrayListPiece.add(imgPiece2);
        arrayListPiece.add(imgPiece3);
        arrayListPiece.add(imgPiece4);
        arrayListPiece.add(imgPiece5);
        arrayListPiece.add(imgPiece6);
        arrayListPiece.add(imgPiece7);
        arrayListPiece.add(imgPiece8);
        arrayListPiece.add(imgPiece9);


        img1 = (ImageView) findViewById(R.id.imageView1);
        img2 = (ImageView) findViewById(R.id.imageView2);
        img3 = (ImageView) findViewById(R.id.imageView3);
        img4 = (ImageView) findViewById(R.id.imageView4);
        img5 = (ImageView) findViewById(R.id.imageView5);
        img6 = (ImageView) findViewById(R.id.imageView6);
        img7 = (ImageView) findViewById(R.id.imageView7);
        img8 = (ImageView) findViewById(R.id.imageView8);
        img9 = (ImageView) findViewById(R.id.imageView9);

        arrayList = new ArrayList<>();
        arrayList.add(img1);
        arrayList.add(img2);
        arrayList.add(img3);
        arrayList.add(img4);
        arrayList.add(img5);
        arrayList.add(img6);
        arrayList.add(img7);
        arrayList.add(img8);
        arrayList.add(img9);

        Random rndX = new Random();
        Random rndY = new Random();
        for (int i = 0; i < 9; ) {
            int X = rndX.nextInt(374);
            int Y = rndY.nextInt(530);
            if (Y >= 260) {
                arrayListPiece.get(i).setX(X);
                arrayListPiece.get(i).setY(Y);
                i++;
            } else {
                Y = rndY.nextInt(393);
            }
        }

        imageTouch();


        /*imgPiece.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {

                ClipData.Item item = new ClipData.Item((CharSequence) v.getTag());

                String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
                ClipData dragData = new ClipData((CharSequence) v.getTag(), mimeTypes, item);

                myShadow = new MyDragShadowBuilder(imgPiece);

                v.startDrag(dragData, myShadow, null, 0);

                return true;
            }

        });

        imgPiece.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        System.out.println("bat dau keo");
                        layoutParams = (RelativeLayout.LayoutParams) v
                                .getLayoutParams();
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED:
                        System.out.println("dang keo");
                        int x = (int) event.getX();
                        int y = (int) event.getY();

                        break;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        x = (int) event.getX();
                        y = (int) event.getY();
                        break;

                    case DragEvent.ACTION_DROP:
                        System.out.println("nha ra roi");
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        System.out.println("ra ngoai roi");
                        x = (int) event.getX();
                        y = (int) event.getY();
                        layoutParams.leftMargin = x;
                        layoutParams.topMargin = y;
                        v.setLayoutParams(layoutParams);

                        break;
                    case DragEvent.ACTION_DRAG_ENDED:
                        System.out.println("ket thuc roi");

                        break;
                    default:
                        break;
                }

                return true;
            }
        });*/

    }

    private void imageTouch() {

        for (i = 0; i < 9; i++) {
            final ImageView imgPiece = arrayListPiece.get(i);
            final ImageView img = arrayList.get(i);
            imgPiece.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            if (!arrayListCheck.contains(imgPiece)) {
                                status = 0;
                            } else {
                                status = 1;
                            }
                            break;
                        case MotionEvent.ACTION_UP:
                            if (imgPiece.getX() == img.getX() && imgPiece.getY() == img.getY() && !arrayListCheck.contains(imgPiece)) {
                                arrayListCheck.add(imgPiece);
                                tg.startTone(ToneGenerator.TONE_CDMA_SOFT_ERROR_LITE);
                                count++;
                            }
                            if (count == 9) {
                                txtCongratulation.setVisibility(View.VISIBLE);
                                chronometer.stop();
                                long elapsedMillis = SystemClock.elapsedRealtime() - chronometer.getBase();




                            }
                            break;
                        case MotionEvent.ACTION_MOVE:
                            if (status == 0) {

                                int x = (int) (event.getRawX() - imgPiece.getWidth() / 2);
                                int y = (int) (event.getRawY() - imgPiece.getHeight() / 2);

                                imgPiece.setX(x);
                                imgPiece.setY(y);

                                int X = (int) img.getX();
                                int Y = (int) img.getY();

                                if ((x >= X - 20 && x <= X + 20) && (y >= Y - 20 && y <= Y + 20)) {
                                    imgPiece.setX(X);
                                    imgPiece.setY(Y);
                                    status = 1;
                                }
                            }
                            break;
                    }
                    return false;
                }
            });
        }
    }
}