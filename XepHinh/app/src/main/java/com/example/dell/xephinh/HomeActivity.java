package com.example.dell.xephinh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnStart, btnHighscores, btnSettings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnStart = (Button) findViewById(R.id.buttonStart);
        btnStart.setOnClickListener(this);

        btnHighscores = (Button) findViewById(R.id.buttonHighscores);
        btnHighscores.setOnClickListener(this);

        btnSettings = (Button) findViewById(R.id.buttonSettings);
        btnSettings.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonStart:

                Intent intent = new Intent(this, SplashActivity.class);
                startActivity(intent);

                break;
            case R.id.buttonHighscores:

                break;
            case R.id.buttonSettings:

                break;
            default:
                break;
        }

    }
}
