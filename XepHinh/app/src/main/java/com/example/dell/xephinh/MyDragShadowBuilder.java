package com.example.dell.xephinh;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.RelativeLayout;


public class MyDragShadowBuilder extends View.DragShadowBuilder {

    private static Drawable shadow;

    public MyDragShadowBuilder(View v) {
        super(v);

        v.setDrawingCacheEnabled(true);
        Bitmap bm = v.getDrawingCache();
        shadow = new BitmapDrawable(bm);
        shadow.setColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY);

    }

    @Override
    public void onProvideShadowMetrics(Point size, Point touch) {

        int width, height;
        width = getView().getWidth();
        height = getView().getHeight();
        shadow.setBounds(0, 0, width, height);
        size.set(width, height);
        touch.set(width / 2, height / 2);

    }

    @Override
    public void onDrawShadow(Canvas canvas) {
        shadow.draw(canvas);
    }
}
