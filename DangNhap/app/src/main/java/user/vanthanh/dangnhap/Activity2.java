package user.vanthanh.dangnhap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by DELL on 10/22/2015.
 */
public class Activity2 extends Activity {
    EditText edt5, edt;
    Button btn7;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
        final AlertDialog.Builder builder = new AlertDialog.Builder(Activity2.this);
        builder.setTitle("Thông Báo");
        edt = (EditText) findViewById(R.id.editText);
        Intent caller1 = getIntent();
        Bundle callerBundle = caller1.getBundleExtra("MyGoi");
        edt.setText(callerBundle.getString("DangNhap").toString());
        btn7 = (Button) findViewById(R.id.button7);
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                builder.setMessage("Bạn có chắc chắn muốn thoát chương trình!");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent_DangXuat = new Intent(Activity2.this, MainActivity.class);
                        startActivity(intent_DangXuat);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.create().show();
            }
        });
    }
}
