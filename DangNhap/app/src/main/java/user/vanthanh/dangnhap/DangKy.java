package user.vanthanh.dangnhap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import android.widget.EditText;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;


public class DangKy extends Activity {
    EditText edt3, edt4, edt5;
    Button btn3, btn4;
    CheckBox ckbox1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dangky);
        Intent intent;
        final AlertDialog.Builder builder = new AlertDialog.Builder(DangKy.this);
        builder.setTitle("Thông Báo");
        ckbox1 = (CheckBox) findViewById(R.id.checkBox1);
        edt3 = (EditText) findViewById(R.id.editText3);
        edt4 = (EditText) findViewById(R.id.editText4);
        edt5 = (EditText) findViewById(R.id.editText5);
        btn3 = (Button) findViewById(R.id.button3);
        btn3.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {


                                        if (((edt4.getText() + "") == "") | ((edt5.getText() + "") == "")) {

                                            builder.setMessage("Vui lòng nhập mật khẩu");
                                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            });
                                            builder.create().show();
                                        }

                                        if (!(edt4.getText().toString().equals(edt5.getText().toString()))) {
                                            builder.setMessage("Mật khẩu không khớp. Vui lòng nhập lại ");
                                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            });
                                            builder.create().show();
                                        }
                                        if ((edt3.getText() + "") == "") {
                                            builder.setMessage("Vui lòng nhập vào tên tài khoản.");
                                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            });
                                            builder.create().show();
                                        }
                                        boolean kiemtra = false;
                                        for (int i = 0; i < edt3.getText().toString().length(); i++) {
                                            if (!((int) (edt3.getText().toString().charAt(i)) >= 97 && (int) (edt3.getText().toString().charAt(i)) <= 122)
                                                    && !((int) (edt3.getText().toString().charAt(i)) >= 48 && (int) (edt3.getText().toString().charAt(i)) <= 57)
                                                    && !((int) (edt3.getText().toString().charAt(i)) >= 65 && (int) (edt3.getText().toString().charAt(i)) <= 90)) {
                                                kiemtra = true;
                                                builder.setMessage("Tài khoản không được chứa các ký tự đặc biệt");
                                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.cancel();
                                                    }
                                                });
                                                builder.create().show();
                                                return;
                                            }
                                        }

                                        try {
                                            String sdcard = Environment
                                                    .getExternalStorageDirectory()
                                                    .getAbsolutePath() + "/taikhoan.txt";
                                            String data1 = "";
                                            int i;
                                            FileReader in = new FileReader(sdcard);
                                            while ((i = in.read()) != -1)
                                                data1 += (char) i;
                                            in.close();
                                            if (data1.contains("Tai Khoan[" + " Name = " + edt3.getText().toString())) {
                                                builder.setMessage("Tài khoản đã có người đăng ký. Vui lòng chọn tài khoản mới.");
                                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.cancel();
                                                    }
                                                });
                                                builder.create().show();
                                            } else {
                                                boolean test = ckbox1.isChecked();
                                                if ((((edt4.getText() + "") != "")) && (kiemtra == false) && (test == true) && (edt4.getText().toString().equals(edt5.getText().toString()))) {
                                                    String sdcard1 = Environment
                                                            .getExternalStorageDirectory()
                                                            .getAbsolutePath() + "/taikhoan.txt";
                                                    try {
                                                        FileOutputStream out = new FileOutputStream(sdcard1, true);
                                                        OutputStreamWriter writer =
                                                                new OutputStreamWriter(out);
                                                        TaiKhoan tk1 = new TaiKhoan();
                                                        tk1.ten = edt3.getText().toString();
                                                        tk1.matkhau = edt4.getText().toString();
//                                                Intent intent = new Intent(DangKy.this, DangNhap.class);
//                                                Bundle bundle = new Bundle();
//                                                bundle.putSerializable("mang",tk);
//                                                intent.putExtra("Goi", bundle);
                                                        writer.write(tk1.toString());
                                                        writer.close();
                                                        builder.setMessage("Đăng ký thành công.Nhấn OK để quay về màn hình chính");
                                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                Intent intent_Login = new Intent(DangKy.this, MainActivity.class);
                                                                startActivity(intent_Login);
//                                                        Bundle bundle = new Bundle();
//                                                        bundle.putString("account", edt3.getText().toString());
//                                                        intent2.putExtra("MyPackage", bundle);
//                                                        startActivity(intent2);
                                                                dialog.cancel();
                                                            }
                                                        });
                                                        builder.create().show();
                                                    } catch (FileNotFoundException e) {
                                                        e.printStackTrace();
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }

                                                }
                                            }

                                        } catch (FileNotFoundException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                }
        );

        btn4 = (Button) findViewById(R.id.button4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                System.out.println("thanh " + arrlist.get(0).ten);
                builder.setMessage("Bạn có chắc chắn muốn hủy và quay trở về màn hình chính");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intentHuy = new Intent(DangKy.this, MainActivity.class);
                        startActivity(intentHuy);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.create().show();
            }
        });
    }
}
