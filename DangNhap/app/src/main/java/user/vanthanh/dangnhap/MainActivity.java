package user.vanthanh.dangnhap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText edt1;
    Button btn2, btn3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edt1 = (EditText) findViewById(R.id.editText5);
//        Intent caller=getIntent();
//        Bundle callBundle=caller.getBundleExtra("MyPackage");
//        edt1.setText(callBundle.getString("account").toString());
        btn3 = (Button) findViewById(R.id.button3);
        btn2 = (Button) findViewById(R.id.button2);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                try {
//                    FileOutputStream out = openFileOutput("textVT1.txt",MODE_APPEND);
//                    OutputStreamWriter writer = new OutputStreamWriter(out);
//                    writer.write(edt1.getText().toString());
//                    writer.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

                Intent intentLogin = new Intent(MainActivity.this, DangNhap.class);
                startActivity(intentLogin);

            }

        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DangKy.class);
                startActivity(intent);
            }
        });
    }
}

