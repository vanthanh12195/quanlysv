package user.vanthanh.dangnhap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

public class DangNhap extends Activity {
    Button btn5, btn6;
    EditText edt6, edt7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dangnhap);
        final AlertDialog.Builder builder = new AlertDialog.Builder(DangNhap.this);
        builder.setTitle("Thông Báo");
        edt6 = (EditText) findViewById(R.id.editText6);
        edt7 = (EditText) findViewById(R.id.editText7);
        btn5 = (Button) findViewById(R.id.button5);
        btn6 = (Button) findViewById(R.id.button6);
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.setMessage("Bạn có chắc chắn muốn thoát???");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(DangNhap.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.create().show();
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sdcard = Environment
                        .getExternalStorageDirectory()
                        .getAbsolutePath() + "/taikhoan.txt";
                try {
                    Scanner scan = new Scanner(new File(sdcard));
                    String data = "";
                    while (scan.hasNext()) {
                        data += scan.nextLine();
                    }
                    scan.close();
                    TaiKhoan tk = new TaiKhoan();
                    tk.ten = edt6.getText().toString();
                    tk.matkhau = edt7.getText().toString();
                    if (data.toString().contains(tk.toString())) {
                        builder.setMessage("Đăng nhập thành công");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent_DangNhap = new Intent(DangNhap.this, Activity2.class);
                                Bundle bundle1 = new Bundle();
                                bundle1.putString("DangNhap", edt6.getText().toString());
                                intent_DangNhap.putExtra("MyGoi", bundle1);
                                startActivity(intent_DangNhap);
                                dialog.cancel();
                            }
                        });
                        builder.create().show();
                    } else {
                        builder.setMessage("Tài khoản hoặc mật khẩu không chính xác!");
                        builder.setPositiveButton("Đăng Ký", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent_DangKy = new Intent(DangNhap.this, DangKy.class);
                                startActivity(intent_DangKy);
                            }
                        });
                        builder.setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder.create().show();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}