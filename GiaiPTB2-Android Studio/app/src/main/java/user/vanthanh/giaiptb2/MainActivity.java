package user.vanthanh.giaiptb2;
import android.app.Dialog;
import android.view.View.OnClickListener;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

import static user.vanthanh.giaiptb2.R.*;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    TextView txt1, txt2;
    EditText edt1, edt2, edt3, edt4;
    Button bt1, bt2, bt3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edt1 = (EditText) findViewById(R.id.editText1);
        edt2 = (EditText) findViewById(R.id.editText2);
        edt3 = (EditText) findViewById(R.id.editText3);
        edt4 = (EditText) findViewById(R.id.editText4);
        bt3 = (Button) findViewById(R.id.btn3);
        bt3.setOnClickListener(this);
        bt2 = (Button) findViewById(R.id.btn2);
        bt2.setOnClickListener(this);
        bt1 = (Button) findViewById(R.id.btn1);
        bt1.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn3:
                AlertDialog.Builder b = new AlertDialog.Builder(MainActivity.this);
                b.setTitle("Question");
                b.setMessage("Bạn có chắc chắn muốn thoát ứng dụng ?");
                b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });

                b.create().show();
                break;
            case R.id.btn2:
                edt1.setText(null);
                edt2.setText(null);
                edt3.setText(null);
                edt4.setText(null);
                break;
            case R.id.btn1:
                try {
                    double A = Double.parseDouble(edt1.getText().toString());
                    double B = Double.parseDouble(edt2.getText().toString());
                    double C = Double.parseDouble(edt3.getText().toString());
                    double delta = Math.pow(B, 2) - 4 * A * C;
                    if (delta == 0)
                        edt4.setText("x = " + (double) Math.round((-B / (2 * A) * 100) / 100));
                    if (delta < 0)
                        edt4.setText("PT vô nghiệm !");
                    if (delta > 0)
                        edt4.setText("x1 = " + (double) Math.round((-B + Math.sqrt(delta)) / (2 * A) * 100) / 100
                                + ", x2 = " + (double) Math.round((-B - Math.sqrt(delta)) / (2 * A) * 100) / 100);
                    if (A == 0) {
                        if (B == 0 && C != 0)
                            edt4.setText("PT vô nghiệm !");
                        if (B != 0)
                            edt4.setText("x = " + (double) Math.round((-C / B) * 100) / 100);
                    }
                } catch (Exception e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Thông Báo");
                    builder.setMessage("Vui lòng nhập vào các hệ số A,B và C thích hợp");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.create().show();
                }
        }
    }
}







