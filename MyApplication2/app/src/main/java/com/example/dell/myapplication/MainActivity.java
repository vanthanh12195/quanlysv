package com.example.dell.myapplication;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView lvTiGia;
    ArrayList<TiGia> arrayList;
    ItemAdapter adapter;
    URL url;
    HttpURLConnection connection;
    Bitmap bitmap;
    String path = "http://tigia.hunggiasaigon.com/";
    int a = 0;
    ProgressBar progressLoading;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        progressLoading = (ProgressBar) findViewById(R.id.progressBarLoading);
        progressLoading.setVisibility(View.VISIBLE);

        arrayList = new ArrayList<>();

        lvTiGia = (ListView) findViewById(R.id.listViewTiGia);

        ConnectivityManager manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        if (manager.getActiveNetworkInfo() != null) {

        } else {

        }
        getDataFromWeb();
    }

    private void getDataFromWeb() {

        AsyncTask<String, Void, String> asyncTask = new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    url = new URL(params[0]);
                    connection = (HttpURLConnection) url.openConnection();

                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);

                    connection.connect();
                    InputStream in = connection.getInputStream();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder builder = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line).append("\n");
                    }
                    in.close();
                    parseJson(builder.toString());
                    return builder.toString();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                adapter = new ItemAdapter(MainActivity.this, R.layout.activity_item_adapter, arrayList);
                lvTiGia.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                progressLoading.setVisibility(View.GONE);
            }
        };
        asyncTask.execute(new String[]{path});
    }

    private void parseJson(String s) {
        try {
            JSONObject object = new JSONObject(s);
            JSONArray array = object.getJSONArray("items");
            for (int i = 0; i < array.length(); i++) {
                Items loaiCk = new Gson().fromJson(array.get(i).toString(), Items.class);

                TiGia tiGia = new TiGia();

                tiGia.setType(loaiCk.getType());
                tiGia.setMuatienmat(loaiCk.getMuatienmat());
                tiGia.setMuack(loaiCk.getMuack());
                tiGia.setBantienmat(loaiCk.getBantienmat());
                tiGia.setBanck(loaiCk.getBanck());

                url = new URL(loaiCk.getImageurl());
                connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setDoInput(true);

                connection.connect();
                InputStream in = connection.getInputStream();
                Bitmap bitmap1 = BitmapFactory.decodeStream(in);

                in.close();
                tiGia.setBm(bitmap1);

                arrayList.add(tiGia);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadImage(String s) {
        AsyncTask<String, Void, Bitmap> asyncTask1 = new AsyncTask<String, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(String... params) {
                try {
                    url = new URL(params[0]);
                    connection = (HttpURLConnection) url.openConnection();

                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);

                    connection.connect();
                    InputStream in = connection.getInputStream();
                    Bitmap bitmap1 = BitmapFactory.decodeStream(in);

                    in.close();
                    arrayList.get(a).setBm(bitmap1);
                    a++;
                    return bitmap1;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bm) {
                super.onPostExecute(bitmap);
                progressLoading.setVisibility(View.GONE);
            }
        };
        asyncTask1.execute(new String[]{s});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {

        }
        return super.onOptionsItemSelected(item);
    }
}
