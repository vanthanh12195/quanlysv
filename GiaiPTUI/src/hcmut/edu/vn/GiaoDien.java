package hcmut.edu.vn;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.text.StyledEditorKit.AlignmentAction;
import javax.swing.text.html.Option;

public class GiaoDien extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Icon icon1=new ImageIcon("hinh.jpg");
	public void showUI(){
		this.setVisible(true);
		this.setResizable(false);
		this.setSize(300, 230);
		this.setTitle("Giải PTB2");
		this.setLocationRelativeTo(null);
	}
	JLabel label1,label2,label3,label4,label5,label6,label7;
	JButton button1,button2,button3;
	JTextField hsA,hsB,hsC,kq;
	JPanel pn;
	GridLayout grid;
	public void addComponent(){	
		Container con=getContentPane();
		pn=new JPanel();
		pn.setLayout(new BorderLayout());
		con.add(pn);

		JPanel pnNorth=new JPanel();
		JPanel pnNorth1=new JPanel();
		pnNorth.setLayout(new BoxLayout(pnNorth, BoxLayout.Y_AXIS));
		label1=new JLabel("GIẢI PHƯƠNG TRÌNH BẬC 2");
		label2=new JLabel("Nhập vào các hệ số A, B và C");
		pnNorth.add(label1);
		pnNorth.add(label2);
		label1.setForeground(Color.BLUE);
		label1.setFont(new Font("Times New Roman", Font.BOLD|Font.ITALIC, 19));
		label2.setFont(new Font("Times New Roman",Font.PLAIN, 16));
		label2.setAlignmentX(Component.CENTER_ALIGNMENT);
		label1.setAlignmentX(Component.CENTER_ALIGNMENT);

		pn.add(pnNorth,BorderLayout.NORTH);

		JPanel pnCenter=new JPanel();
		pnCenter.setLayout(new BoxLayout(pnCenter, BoxLayout.Y_AXIS));

		JPanel pnCenter1=new JPanel();
		pnCenter1.setLayout(new FlowLayout(10,30,10));
		label3=new JLabel("Hệ Số A");
		hsA=new JTextField(5);
		button1=new JButton("GIẢI");
		pnCenter1.add(label3);
		pnCenter1.add(hsA);
		pnCenter1.add(button1);

		pnCenter.add(pnCenter1);

		JPanel pnCenter2=new JPanel();
		pnCenter2.setLayout(new FlowLayout(10,30,0));
		label4=new JLabel("Hệ Số B");
		hsB=new JTextField(5);
		button2=new JButton("XÓA");
		pnCenter2.add(label4);
		pnCenter2.add(hsB);
		pnCenter2.add(button2);

		pnCenter.add(pnCenter2);

		JPanel pnCenter3=new JPanel();
		pnCenter3.setLayout(new FlowLayout(0,30,10));
		label5=new JLabel("Hệ Số C");
		hsC=new JTextField(5);
		button3=new JButton("THOÁT");
		pnCenter3.add(label5);
		pnCenter3.add(hsC);
		pnCenter3.add(button3);

		pnCenter.add(pnCenter3);

		pn.add(pnCenter,BorderLayout.CENTER);

		JPanel pnSouth=new JPanel();
		pnSouth.setLayout(new FlowLayout(0,30,10));
		label6=new JLabel("KẾT QUẢ");
		kq=new JTextField(14);
		pnSouth.add(label6);
		pnSouth.add(kq);
		
		pn.add(pnSouth, BorderLayout.SOUTH);
		
	}

	public void setButton(){


		button3.addActionListener(new ActionListener() {
			@Override

			public void actionPerformed(ActionEvent e) {

				int i=JOptionPane.showConfirmDialog(null, "Bạn có chắc chắn muốn thoát ứng dụng không?", 
						"Giải PTB2",JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,icon1);
				if(i==0)
					System.exit(0);
				else
					return;
			}
		});
		button2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				hsA.setText("");
				hsB.setText("");
				hsC.setText("");
				kq.setText("");
			}
		});
		button1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e)  {
				Scanner sc=new Scanner(System.in);
				try {
					Double A=Double.parseDouble(hsA.getText().toString());
					Double B=Double.parseDouble(hsB.getText().toString());
					Double C=Double.parseDouble(hsC.getText().toString());
					Double delta= B*B-4*A*C;
					if(delta==0)
						kq.setText("Nghiệm kép là "+(double)Math.round((-B/(2*A))*10)/10);
					if(delta<0)
						kq.setText("PT vô nghiệm !");
					if(delta>0)
						kq.setText("Hai nghiệm là "+(double)Math.round((-B+Math.sqrt(delta))/(2*A)*10)/10
								+" và "+(double)Math.round((-B-Math.sqrt(delta))/(2*A)*10)/10);
					if(A==0){
						if(B==0 && C!=0)
							kq.setText("PT vô nghiệm !");
						if(B!=0)
							kq.setText("PT có nghiệm là "+(double)Math.round((-C/B)*10)/10);
					}
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "VUI LÒNG NHẬP SỐ THÍCH HỢP");
				}


			}

		});
	}

	
}