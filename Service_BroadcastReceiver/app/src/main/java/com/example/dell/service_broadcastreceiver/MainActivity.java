package com.example.dell.service_broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnRegisterBroadcast, btnUnregisterBroadcast, btnSend, btnStartService, btnStopService;
    BroadcastReceiver receiver;
    String KEY = "abc";
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.ListtenEvent);

        btnStartService = (Button) findViewById(R.id.buttonStartService);
        btnStartService.setOnClickListener(this);

        btnStopService = (Button) findViewById(R.id.buttonStopService);
        btnStopService.setOnClickListener(this);

        btnRegisterBroadcast = (Button) findViewById(R.id.buttonRegisterBroadcast);
        btnRegisterBroadcast.setOnClickListener(this);

        btnUnregisterBroadcast = (Button) findViewById(R.id.buttonUnregisterBroadcast);
        btnUnregisterBroadcast.setOnClickListener(this);

        btnSend = (Button) findViewById(R.id.buttonSend);
        btnSend.setOnClickListener(this);

        receiver = new Broadcast1();
       /* final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_POWER_CONNECTED);
        intentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase(intentFilter.getAction(0))) {
                    System.out.println("dang xac pin");
                    textView.setText("dang xac pin");
                }
                if (intent.getAction().equalsIgnoreCase(intentFilter.getAction(1))) {
                    System.out.println("da rut xac pin");
                    textView.setText("da rut xac pin");
                }
            }
        };
        registerReceiver(receiver, intentFilter);*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonRegisterBroadcast:
                IntentFilter intentFilter = new IntentFilter(KEY);
                registerReceiver(receiver, intentFilter);
                break;
            case R.id.buttonSend:
                Intent intent1 = new Intent(KEY);
                intent1.putExtra("data", 12195);
                sendBroadcast(intent1);
                break;
            case R.id.buttonUnregisterBroadcast:
                unregisterReceiver(receiver);
                break;
            case R.id.buttonStartService:
                Intent intent = new Intent(this, Service1.class);
                intent.putExtra("data", "0168 281 8737");
                startService(intent);
                break;
            case R.id.buttonStopService:
                Intent intent2 = new Intent(this, Service1.class);
                stopService(intent2);
                break;
            default:
                break;
        }
    }
   /* @Override
    protected void onDestroy() {
        super.onDestroy();
        // sau khi thoát ứng dụng
        System.out.println(123);
        if (receiver != null) {
            System.out.println("da null roi");
            unregisterReceiver(receiver);
        }
    }*/
}
