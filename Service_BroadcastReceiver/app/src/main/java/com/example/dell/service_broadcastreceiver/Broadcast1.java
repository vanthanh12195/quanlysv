package com.example.dell.service_broadcastreceiver;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class Broadcast1 extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // SQLiteExanple project
        Intent intent1 = new Intent(context, Service1.class);
        intent1.putExtra("goi1", intent.getStringExtra("goi"));
        context.startService(intent1);
    }
}