package com.example.dell.service_broadcastreceiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.security.Provider;

public class Service1 extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("da duoc tao");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final String ss = intent.getStringExtra("goi1") + " ";
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <= 15; i++) {
                    System.out.println(ss + i);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Intent intent1 = new Intent(Service1.this, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(Service1.this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(Service1.this);
                builder.setContentTitle(ss);
                builder.setAutoCancel(true);
                builder.setSmallIcon(R.drawable.ic_launcher);
                builder.setTicker("ban co tin nhan");
                builder.setContentIntent(pendingIntent);

                NotificationManager manager = (NotificationManager) Service1.this.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(1408, builder.build());
            }
        });
        thread.start();


        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(Service1.this, "da tat service", Toast.LENGTH_SHORT).show();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }
}
