package com.example.dell.googlemapsapi;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.camera2.CameraManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout relativeLayout;
    Button btnDirections, btnGetDirections;
    GoogleMap googleMap;
    ProgressDialog progressDialog;
    String path = "http://maps.googleapis.com/maps/api/directions/json?origin=10.769310,106.661368&destination=10.806576,106.690551&language=vi";
    String ss = null;
    ArrayList<DirectionStep> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        relativeLayout = (RelativeLayout) findViewById(R.id.layoutDirections);
        relativeLayout.setVisibility(View.GONE);

        btnGetDirections = (Button) findViewById(R.id.buttonGetDirections);
        btnGetDirections.setOnClickListener(this);

        btnDirections = (Button) findViewById(R.id.buttonDirections);
        btnDirections.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Thông Báo");
        progressDialog.setMessage("Đang tải Map....");
        progressDialog.show();

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);

        googleMap = mapFragment.getMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setMyLocationEnabled(true);

        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                progressDialog.dismiss();
            }
        });


        /*googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location location) {

                        LatLng latLing = new LatLng(location.getLatitude(), location.getLongitude());

                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLing, 15));

                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(latLing);
                        markerOptions.title("Đây là nhà của tôi");
                        markerOptions.snippet("Safe House");

                        googleMap.addMarker(markerOptions);
                    }
                });
                return false;
            }
        });*/
    }

    private void TuiDangODau() {

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        Location lastLocation = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        if (lastLocation != null) {
            LatLng latLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)      // Sets the center of the map to location user
                    .zoom(15)                   // Sets the zoom
                    .bearing(90)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//Thêm MarketOption cho Map:
            MarkerOptions option = new MarkerOptions();
            option.title("Chỗ Tui đang ngồi đó");
            option.snippet("Gần làng SOS");
            option.position(latLng);
            Marker currentMarker = googleMap.addMarker(option);
            currentMarker.showInfoWindow();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonDirections:
                arrayList = new ArrayList<>();
                getDirections();
                break;
            case R.id.buttonGetDirections:
                getDetailDirections();
                break;
            default:
                break;
        }
    }

    private void getDetailDirections() {
        Intent intent = new Intent(this, ListDirection.class);
        Bundle bundle = new Bundle();
        bundle.putString("data", ss);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void getDirections() {
        AsyncTask<String, String, String> asyncTask = new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);

                    connection.connect();

                    System.out.println("chieu dai la " + connection.getContentLength());

                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder builder = new StringBuilder();

                    StringBuffer sb = new StringBuffer();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    reader.close();

                    return sb.toString();

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                super.onProgressUpdate(values);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                relativeLayout.setVisibility(View.VISIBLE);
                ss = s;
                parseJSon(s);
                PolylineOptions lineOption = new PolylineOptions();
                for (int i = 0; i < arrayList.size(); i++) {
                    lineOption.add(arrayList.get(i).getLatLngStart());
                    lineOption.add(arrayList.get(i).getLatLngEnd());
                }
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(arrayList.get(0).getLatLngStart(), 15));
                MarkerOptions options = new MarkerOptions();
                options.position(arrayList.get(0).getLatLngStart());
                options.title("Begin");
                googleMap.addMarker(options);
                options.position(arrayList.get(arrayList.size() - 1).getLatLngEnd());
                options.title("End");
                googleMap.addMarker(options);
                lineOption.width(5);
                lineOption.color(Color.BLUE);
                googleMap.addPolyline(lineOption);
            }
        };
        asyncTask.execute(new String[]{path});
    }

    private void parseJSon(String s) {
        try {

            JSONObject jsonObject = new JSONObject(s);
            JSONArray array = jsonObject.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONArray("steps");
            for (int i = 0; i < array.length(); i++) {

                DirectionStep step = new DirectionStep();
                step.setLatLngStart(new LatLng(array.getJSONObject(i).getJSONObject("start_location").getDouble("lat"),
                        array.getJSONObject(i).getJSONObject("start_location").getDouble("lng")));
                step.setLatLngEnd(new LatLng(array.getJSONObject(i).getJSONObject("end_location").getDouble("lat"),
                        array.getJSONObject(i).getJSONObject("end_location").getDouble("lng")));
                arrayList.add(step);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
