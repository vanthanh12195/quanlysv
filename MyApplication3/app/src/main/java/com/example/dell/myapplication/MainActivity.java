package com.example.dell.myapplication;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    ListView lvTiGia;
    ArrayList<TiGia> arrayList;
    ArrayList<TiGia> arrayList1;
    ItemAdapter adapter;
    ItemAdapter adapter1;
    URL url;
    HttpURLConnection connection;
    String path = "http://tigia.hunggiasaigon.com/";
    ProgressBar progressLoading;
    SearchView searchView;
    StringBuilder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        arrayList = new ArrayList<>();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progressLoading = (ProgressBar) findViewById(R.id.progressBarLoading);
        progressLoading.setVisibility(View.VISIBLE);

        lvTiGia = (ListView) findViewById(R.id.listViewTiGia);

        ConnectivityManager manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        if (manager.getActiveNetworkInfo() != null) {
            getDataFromWeb();
        } else {
            Toast.makeText(MainActivity.this, "Ứng Dụng chỉ hiển thị dữ liệu Offline", Toast.LENGTH_LONG).show();
            getDataFromSQL();
        }
    }

    private void getDataFromWeb() {
        AsyncTask<String, String, String> asyncTask = new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    url = new URL(params[0]);
                    connection = (HttpURLConnection) url.openConnection();

                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);

                    connection.connect();
                    InputStream in = connection.getInputStream();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    builder = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line).append("\n");
                    }
                    in.close();
                    parseJson(builder.toString());
                    return builder.toString();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                adapter = new ItemAdapter(MainActivity.this, R.layout.activity_item_adapter, arrayList);
                lvTiGia.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                progressLoading.setVisibility(View.GONE);

                TiGiaDatabase database = new TiGiaDatabase(MainActivity.this);
                SQLiteDatabase db = database.getWritableDatabase();
                db.delete("table3", null, null);
                for (int i = 0; i < arrayList.size(); i++) {
                    ContentValues values = new ContentValues();
                    values.put("bytesofimage", arrayList.get(i).getBuffer());
                    values.put("type", arrayList.get(i).getType());
                    values.put("muatm", arrayList.get(i).getMuatienmat());
                    values.put("bantm", arrayList.get(i).getBantienmat());
                    values.put("muack", arrayList.get(i).getMuack());
                    values.put("banck", arrayList.get(i).getBanck());
                    db.insert("table3", null, values);
                }
            }
        };
        asyncTask.execute(new String[]{path});
    }

    private void parseJson(String s) {
        try {
            JSONObject object = new JSONObject(s);
            JSONArray array = object.getJSONArray("items");
            for (int i = 0; i < array.length(); i++) {
                Items loaiCk = new Gson().fromJson(array.get(i).toString(), Items.class);

                final TiGia tiGia = new TiGia();

                tiGia.setType(loaiCk.getType());
                tiGia.setMuatienmat(loaiCk.getMuatienmat());
                tiGia.setMuack(loaiCk.getMuack());
                tiGia.setBantienmat(loaiCk.getBantienmat());
                tiGia.setBanck(loaiCk.getBanck());

                URL url = new URL(loaiCk.getImageurl());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setDoInput(true);

                connection.connect();
                InputStream in = connection.getInputStream();

                byte[] buffer = new byte[connection.getContentLength()];
                byte[] arr = new byte[100];
                int index = 0;
                int len = 0;

                while ((len = in.read(arr)) != -1) {
                    for (int j = 0; j < len; j++) {
                        buffer[index] = arr[j];
                        index++;
                    }
                }
                Bitmap bitmap = BitmapFactory.decodeByteArray(buffer, 0, connection.getContentLength());
                in.close();
                tiGia.setBuffer(buffer);
                tiGia.setBm(bitmap);
                arrayList.add(tiGia);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem menuItem = menu.findItem(R.id.searchItem);
        searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_luuoffline) {
            Toast.makeText(MainActivity.this, "Da luu Offline", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getDataFromSQL() {
        TiGiaDatabase database = new TiGiaDatabase(this);
        SQLiteDatabase db = database.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from table3", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            TiGia tiGia = new TiGia();
            tiGia.setBm(BitmapFactory.decodeByteArray(cursor.getBlob(0), 0, cursor.getBlob(0).length));
            tiGia.setType(cursor.getString(1));
            tiGia.setMuatienmat(cursor.getString(2));
            tiGia.setBantienmat(cursor.getString(3));
            tiGia.setMuack(cursor.getString(4));
            tiGia.setBanck(cursor.getString(5));
            arrayList.add(tiGia);
            cursor.moveToNext();
        }
        adapter = new ItemAdapter(MainActivity.this, R.layout.activity_item_adapter, arrayList);
        lvTiGia.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        progressLoading.setVisibility(View.GONE);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            adapter.getFilter().filter("");
            lvTiGia.setAdapter(adapter);
        } else {
            arrayList1 = new ArrayList<>();
            arrayList1.clear();
            adapter1 = new ItemAdapter(MainActivity.this, R.layout.activity_item_adapter, arrayList1);
            lvTiGia.setAdapter(adapter1);
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).getType().toLowerCase().contains(newText.toLowerCase())) {
                    arrayList1.add(arrayList.get(i));
                    adapter1 = new ItemAdapter(MainActivity.this, R.layout.activity_item_adapter, arrayList1);
                    lvTiGia.setAdapter(adapter1);
                }
            }
        }
        return true;

    }
}
