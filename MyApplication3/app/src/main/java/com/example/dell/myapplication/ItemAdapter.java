package com.example.dell.myapplication;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ItemAdapter extends ArrayAdapter implements Filterable {
    Activity activity;
    int layoutId;
    ArrayList<TiGia> arrayList;

    public ItemAdapter(Activity activity, int layoutId, ArrayList<TiGia> arrayList) {
        super(activity, layoutId, arrayList);
        this.activity = activity;
        this.layoutId = layoutId;
        this.arrayList = arrayList;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = View.inflate(activity, R.layout.activity_item_adapter, null);

        TextView txtType = (TextView) view.findViewById(R.id.textViewType);
        txtType.setText(arrayList.get(position).getType());

        TextView txtMuaTM = (TextView) view.findViewById(R.id.textViewMuaTM);
        txtMuaTM.setText(arrayList.get(position).getMuatienmat());

        TextView txtMuaCK = (TextView) view.findViewById(R.id.textViewMuaCK);
        txtMuaCK.setText(arrayList.get(position).getMuack());

        TextView txtBanTM = (TextView) view.findViewById(R.id.textViewBanTM);
        txtBanTM.setText(arrayList.get(position).getBantienmat());

        TextView txtBanCK = (TextView) view.findViewById(R.id.textViewBanCK);
        txtBanCK.setText(arrayList.get(position).getBanck());

        ImageView img = (ImageView) view.findViewById(R.id.imageView);
        img.setImageBitmap(arrayList.get(position).getBm());

        return view;
    }
}
