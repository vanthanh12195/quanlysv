package com.example.dell.news;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

public class BaseFragment {
    Activity activity;
    Fragment fragment;
    String key;
    Boolean aBoolean;

    public BaseFragment(Activity activity, Fragment fragment, String key, Boolean aBoolean) {
        this.activity = activity;
        this.fragment = fragment;
        this.key = key;
        this.aBoolean = aBoolean;
    }

    public void AddFragment() {

        FragmentManager fragManager = activity.getFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        fragTransaction.add(R.id.layoutContain, fragment);
        if (aBoolean) {
            fragTransaction.addToBackStack(key);
        }
        fragTransaction.commit();

    }
}
