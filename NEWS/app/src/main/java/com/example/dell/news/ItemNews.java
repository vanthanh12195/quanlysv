package com.example.dell.news;

import android.graphics.Bitmap;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by DELL on 19/01/2016.
 */
public class ItemNews  {
    private Bitmap bitmapNews;
    private String textViewTitle;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Bitmap getBitmapNews() {
        return bitmapNews;
    }

    public void setBitmapNews(Bitmap bitmapNews) {
        this.bitmapNews = bitmapNews;
    }

    public String getTextViewTitle() {
        return textViewTitle;
    }

    public void setTextViewTitle(String textViewTitle) {
        this.textViewTitle = textViewTitle;
    }

}
