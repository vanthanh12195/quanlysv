package com.example.dell.news;

/**
 * Created by DELL on 19/01/2016.
 */
public enum FragmentKey {
    HOME("home"),
    DETAILS("details");
    private String value;

    public String getValue() {
        return value;
    }

    FragmentKey(String value) {
        this.value = value;
    }
}
