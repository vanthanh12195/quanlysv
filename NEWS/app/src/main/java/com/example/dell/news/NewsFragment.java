package com.example.dell.news;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class NewsFragment extends Fragment {

    ProgressBar progressBarLoading;
    String xmlString = null;
    String path = "http://www.24h.com.vn/upload/rss/bongda.rss";
    TextView txt01;
    ListView listViewXml;
    ItemLists adapter;
    ArrayList<ItemNews> arrayList;
    InputStream inputStream;
    InputStream in;
    Bitmap bm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.news_fragment, null, false);

        progressBarLoading = (ProgressBar) view.findViewById(R.id.progressBarLoading);
        progressBarLoading.setVisibility(View.VISIBLE);

        listViewXml = (ListView) view.findViewById(R.id.listViewXML);
        listViewXml.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Bundle bundle = new Bundle();
                bundle.putString("url", arrayList.get(position).getUrl());

                Fragment detailNews = new DetailNews();
                detailNews.setArguments(bundle);
                BaseFragment baseFragment = new BaseFragment(getActivity(), detailNews, FragmentKey.DETAILS.getValue(), true);
                baseFragment.AddFragment();
            }
        });

        AsyncTask<Void, Void, Void> asyncTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    URL url = new URL(path);

                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    /*connection.setRequestProperty("Content-Type", "application/xml");
                    connection.setRequestProperty("User-Agent", "Fiddler");*/

                    connection.setDoInput(true);
                    connection.setRequestMethod("GET");

                    connection.connect();

                    inputStream = connection.getInputStream();

                    parseXml();

                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder builder = new StringBuilder();
                    String line = null;

                    while ((line = bufferedReader.readLine()) != null) {
                        builder.append(line).append("\n");
                    }

                    inputStream.close();


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }


            @Override
            protected void onPostExecute(Void avoid) {
                super.onPostExecute(avoid);

                progressBarLoading.setVisibility(View.GONE);
                System.out.println("chieu dai la " + arrayList.size());
                adapter = new ItemLists(getActivity(), R.layout.activity_list_item_news_paper, arrayList);
                listViewXml.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }
        };

        asyncTask.execute();
        return view;

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void parseXml() {
        arrayList = new ArrayList<>();
        try {

            XmlPullParserFactory parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();
            parser.setInput(inputStream, "UTF-8");

            int eventType = -1;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                eventType = parser.next();
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.END_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        if (parser.getName().equalsIgnoreCase("item")) {
                            ItemNews itemNews = new ItemNews();

                            parser.nextTag();
                            parser.next();
                            itemNews.setTextViewTitle(parser.getText());

                            parser.nextTag();
                            parser.nextTag();
                            parser.next();
                            String cdata = parser.getText();
                            String url1 = cdata.substring(cdata.indexOf("src='") + 5, cdata.indexOf(" alt='") - 1);
                            URL url = new URL(url1);
                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                            connection.setRequestMethod("GET");
                            connection.setDoInput(true);
                            connection.connect();
                            in = connection.getInputStream();
                            bm = BitmapFactory.decodeStream(in);
                            in.close();
                            itemNews.setBitmapNews(bm);

                            parser.nextTag();
                            parser.nextTag();
                            parser.next();
                            parser.nextTag();
                            parser.nextTag();
                            parser.next();
                            itemNews.setUrl(parser.getText());

                            arrayList.add(itemNews);
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        break;
                    default:
                        break;
                }
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
