package com.example.dell.news;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment fragment = new NewsFragment();
        BaseFragment baseFragment = new BaseFragment(this, fragment, FragmentKey.HOME.getValue(),false);
        baseFragment.AddFragment();

    }

}
