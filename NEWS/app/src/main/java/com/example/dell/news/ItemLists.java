package com.example.dell.news;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ItemLists extends ArrayAdapter {


    Activity activity;
    int layoutId;
    ArrayList<ItemNews> arrayList;

    public ItemLists(Activity activity, int layoutId, ArrayList<ItemNews> arrayList) {
        super(activity, layoutId, arrayList);
        this.activity = activity;
        this.layoutId = layoutId;
        this.arrayList = arrayList;
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = View.inflate(activity, R.layout.activity_list_item_news_paper, null);

        ImageView imgNews = (ImageView) view.findViewById(R.id.imageViewNews);
        TextView txtTitle = (TextView) view.findViewById(R.id.textViewTitle);

        imgNews.setImageBitmap(arrayList.get(position).getBitmapNews());
        txtTitle.setText(arrayList.get(position).getTextViewTitle());


        return view;
    }

}
