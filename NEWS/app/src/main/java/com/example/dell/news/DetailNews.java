package com.example.dell.news;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import java.security.Key;

public class DetailNews extends Fragment {

    WebView webViewDetailNews;
    ProgressBar pBarWebview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_news, null, false);

        pBarWebview = (ProgressBar) view.findViewById(R.id.progressBarWebview);
        pBarWebview.setVisibility(View.VISIBLE);
        pBarWebview.setMax(100);

        webViewDetailNews = (WebView) view.findViewById(R.id.webViewDetailNews);
        webViewDetailNews.getSettings().setJavaScriptEnabled(true);
        webViewDetailNews.loadUrl(getArguments().getString("url"));
        webViewDetailNews.setWebViewClient(new MyWebView());
        webViewDetailNews.setWebChromeClient(new WebChromeClient() {
            

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                System.out.println(newProgress);
                pBarWebview.setProgress(newProgress);
            }
        });

        return view;
    }

    public class MyWebView extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            pBarWebview.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            pBarWebview.setVisibility(View.INVISIBLE);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return super.shouldOverrideUrlLoading(view, url);
        }

    }

}
