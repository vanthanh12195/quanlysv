package com.example.dell.myapplication;

import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by DELL on 19/01/2016.
 */
public class ItemNews {
    private Bitmap bitmapNews;
    private String textViewTitle;

    public Bitmap getBitmapNews() {
        return bitmapNews;
    }

    public void setBitmapNews(Bitmap bitmapNews) {
        this.bitmapNews = bitmapNews;
    }

    public String getTextViewTitle() {
        return textViewTitle;
    }

    public void setTextViewTitle(String textViewTitle) {
        this.textViewTitle = textViewTitle;
    }


}
