package com.example.dell.sqliteexample;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnThemLopHoc, btnDSLopHoc, btnSend;
    LopHocDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSend = (Button) findViewById(R.id.buttonSend);
        btnSend.setOnClickListener(this);

        btnDSLopHoc = (Button) findViewById(R.id.buttonDSLopHoc);
        btnDSLopHoc.setOnClickListener(this);

        btnThemLopHoc = (Button) findViewById(R.id.buttonThemLopHoc);
        btnThemLopHoc.setOnClickListener(this);

        db = new LopHocDatabase(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonThemLopHoc:
                themLopHoc();
                break;
            case R.id.buttonDSLopHoc:
                dsLopHoc();
                break;
            case R.id.buttonSend:
                Intent intent = new Intent("abc");
                intent.putExtra("goi", "lap trinh tren thiet di di dong K217T26");
                sendBroadcast(intent);
                break;
            default:
                break;
        }


    }

    private void dsLopHoc() {
        SQLiteDatabase database = db.getReadableDatabase();

        /*String query = "SELECT * FROM AndroidK217T26 ORDER BY NamHoc ASC";
        Cursor cursor = database.rawQuery(query, null);*/

        Cursor cursor = database.query(false, "AndroidK217T26", new String[]{"TenLop", "NamHoc"}, null, null, null, null, "NamHoc DESC", "6");


        if (!cursor.isFirst()) {
            cursor.moveToFirst();
        }
        while (!cursor.isLast()) {
            System.out.println(cursor.getString(0) + "  " + cursor.getInt(1));
            cursor.moveToNext();
        }
    }

    private void themLopHoc() {

        Random rnd = new Random();
        String tenlop = "Lop " + rnd.nextInt(10) + 1;
        int namhoc = rnd.nextInt(3000);

        SQLiteDatabase database = db.getWritableDatabase();

        /*String insertLopHoc = "INSERT INTO lophocandroid (tenlop,namhoc)" + "VALUES (\"" + tenlop + "\"," + namhoc + ");";
        database.execSQL(insertLopHoc);*/

        ContentValues values = new ContentValues();
        values.put("TenLop", tenlop);
        values.put("NamHoc", namhoc);

        long id = database.insert("AndroidK217T26", null, values);
        System.out.println(id);

    }
}
