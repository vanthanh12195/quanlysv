package com.example.dell.sqliteexample;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class LopHocDatabase extends SQLiteOpenHelper {

    private static final String DATABASENAME = "QLHS1";
    private static int DATABASEVERSION = 1;

    public LopHocDatabase(Context context) {
        super(context, DATABASENAME, null, DATABASEVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        System.out.println("onCreate");
        String SQliteCreateTable = "CREATE TABLE [AndroidK217T26] (MaLop integer NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,TenLop text NOT NULL,NamHoc integer NOT NULL)";
        db.execSQL(SQliteCreateTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        System.out.println("onUpgrade " + oldVersion + " va " + newVersion);

    }
}
